 @php 
    $utm_data = json_decode($tracking->utm_data)
@endphp

<!-- Hashcode Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('hashcode', 'Hashcode:') !!}
    <p>{{ $tracking->hashcode }}</p>
</div>

<!-- Actual Url Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('actual_url', 'Actual Url:') !!}
    <p>{{ $tracking->actual_url }}</p>
</div>


<!-- Created At Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $tracking->created_at }}</p>
</div>


<!-- Returned At Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('returned_at', 'Returned At:') !!}
    <p>{{ $tracking->returned_at }}</p>
</div>

<!-- Utm Data Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('utm_data', 'Utm Source:') !!}
    <p>{{ $utm_data->utm_source }}</p>
</div>


<!-- Utm Data Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('utm_data', 'Utm medium:') !!}
    <p>{{ $utm_data->utm_medium }}</p>
</div>


<!-- Utm Data Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('utm_data', 'Utm campaign:') !!}
    <p>{{ $utm_data->utm_campaign }}</p>
</div>


<!-- Utm Data Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('utm_data', 'Utm reference:') !!}
    <p>{{ $utm_data->utm_reference }}</p>
</div>
