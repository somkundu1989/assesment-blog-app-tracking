<div class="table-responsive">
    <table class="table" id="trackings-table">
        <thead>
            <tr>
                <th>Actual Url</th>
                <th>created At</th>
                <th>Returned At</th>
                <th>Utm source</th>
                <th>Utm medium</th>
                <th>Utm campaign</th>
                <th>Utm reference</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($trackings as $tracking)
            @php 
                $utm_data = json_decode($tracking->utm_data)
            @endphp
            <tr>
            <td>{{ $tracking->actual_url }}</td>
            <td>{{ $tracking->created_at }}</td>
            <td>{{ $tracking->returned_at }}</td>
            <td>{{ $utm_data->utm_source }}</td>
            <td>{{ $utm_data->utm_medium }}</td>
            <td>{{ $utm_data->utm_campaign }}</td>
            <td>{{ $utm_data->utm_reference }}</td>
                <td>
                    <div class='btn-group'>
                        <a href="{{ route('trackings.show', [$tracking->hashcode]) }}" class='btn btn-default'>View</a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
