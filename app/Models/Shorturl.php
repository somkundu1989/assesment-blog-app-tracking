<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Entry
 * @package App\Models
 * @version March 11, 2020, 6:43 pm UTC
 *
 * @property \App\Models\User user
 * @property \Illuminate\Database\Eloquent\Collection entryTags
 * @property integer user_id
 * @property string entry
 * @property string entry_date
 */
class Shorturl extends Model
{

    public $timestamps = false;
    public $table = 'shorturls';
    const CREATED_AT = 'created_at';



    public $fillable = [
        'created_at',
        'hashcode',
        'actual_url',
        'utm_data',
        'returned_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime',
        'hashcode'=>'string',
        'actual_url'=>'string',
        'utm_data'=>'string',
        'returned_at'=>'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'created_at' => 'required',
        'hashcode' => 'required',
        'actual_url' => 'required',
        'utm_data' => 'required',
        'returned_at' => 'required'
    ];

}
