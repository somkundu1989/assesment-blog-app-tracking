<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Shorturl;
use Hash;

class UrlmodifierController extends Controller
{
    //


    public function encode(string $slugOrId, String $medium, String $campaign,String $reference)
    {
    	$key = env('APP_KEY');
    	$onlyUrl=route('frontend.detail', [$slugOrId]);

        $now = date("Y-m-d H:i:s");
        $hashcode = str_replace('/','-',Hash::make($now.$slugOrId.$medium.$campaign.$reference.rand(1,10000000)));

        Shorturl::create(['actual_url' => $onlyUrl,
			        	'utm_data' => json_encode(['utm_source'=> base64_decode($reference),'utm_medium'=>$medium,'utm_campaign'=>$campaign,'utm_reference'=>null]),
			        	'hashcode' => $hashcode,
			       		'created_at' => $now,
			       		'returned_at' => NULL,
			       	]);
        echo '<a href="'.route('decode-url', [$hashcode]).'">'.route('decode-url', [$hashcode]).'</a>';die;

    }

    public function decode($hashcode)
    {
        /** @var Entry $entry */
        $shorturl = Shorturl::where('hashcode',$hashcode)->first();
        
        if (empty($shorturl)) {

            Flash::error('url not found');

            return redirect(route('frontend.index'));
        }

        $now = date("Y-m-d H:i:s");
        Shorturl::where('hashcode',$hashcode)->update(['returned_at' => $now]);



        return redirect($shorturl->actual_url);
    }
}
