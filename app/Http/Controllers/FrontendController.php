<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Entry as Blog;
use Flash;

class FrontendController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $entries = Blog::orderBy('entry_date', 'desc')->paginate(env('PAGINATION_HOME'));
        return view('frontend.index')
            ->with('entries', $entries);
    }



    /**
     * Display the specified Entry.
     *
     * @param int $id
     *
     * @return Response
     */
    public function detail($idOrSlug)
    {
        /** @var Entry $entry */
        $entry = Blog::where('slug',$idOrSlug)->orWhere('id',$idOrSlug)->first();
        
        if (empty($entry)) {

            Flash::error('Blog not found');

            return redirect(route('frontend.index'));
        }

        return view('frontend.show')->with('entry', $entry);
    }
}
