<div class="row">
        @foreach($entries as $entry)

            <div class="form-group col-2 {{$entry->deleted_at!=null?'deleted':''}}">
                <div class="card">
                  <div class="card-body">
                    <div class="card-text">
                        <div class="clearfix">
                            <label>Date :</label> 
                            <span>{{$entry->entry_date->format('Y-m-d')}}</span>
                        </div>
                        <div class="clearfix">
                            <span>{{substr($entry->title,0,50)}}{{(strlen($entry->title)>50)?'...':''}}</span>
                        </div>
                        <div class="clearfix {{$entry->deleted_at!=null?'span-deleted':''}}">
                            
                                <a href="{{ route('frontend.detail', [$entry->slug ]) }}" class='btn btn-primary'>View</a>
        
                                @include('frontend.action')
                               
                            </span>
                        </div>                                  
                    </div>
                  </div>
                </div>  
            </div>  
        @endforeach
</div>

@section('css')
<style>
.deleted .card{
    background-color: #80808030;
    -webkit-text-decoration-line: line-through;
    text-decoration-line: line-through;
}
.deleted .span-deleted{
    text-decoration: none;
    display: inline-block;
}
.card-body{
    min-height: 201px;
}
</style>
@endsection