<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontendController@index')->name('frontend.index');
Route::get('{SlugOrId}/detail', 'FrontendController@detail')->name('frontend.detail');
Route::get('{SlugOrId}/{medium}/{campaign}/{reference}/url-encode/', 'UrlmodifierController@encode')->name('encode-url');
Route::get('{HashCode}/url-decode/', 'UrlmodifierController@decode')->name('decode-url');
Auth::routes();
Route::middleware(['auth'])->group(function () {
	Route::get('/home', 'HomeController@index')->name('home');
	Route::resource('entries', 'EntryController');

	Route::resource('trackings', 'TrackingController')->only([
	    'index', 'show'
	]);
});