@php

@endphp
<div class="form-group col-sm-12">
    <a target="_blank" href="{{ route('encode-url', [$entry->slug, 'email', 'email-cam-aug-20', base64_encode(URL::current())]) }}" class='btn btn-info'>Email</a>
    <a target="_blank" href="{{ route('encode-url', [$entry->slug, 'whatsapp', 'whatsapp-cam-aug-20', base64_encode(URL::current())]) }}" class='btn btn-info'>WhatsApp</a>
    <a target="_blank" href="{{ route('encode-url', [$entry->slug, 'facebook', 'fb-cam-aug-20', base64_encode(URL::current())]) }}" class='btn btn-info'>Facebook</a>
    <a target="_blank" href="{{ route('encode-url', [$entry->slug, 'instagram', 'instagram-cam-aug-20', base64_encode(URL::current())]) }}" class='btn btn-info'>Instagram</a>
    <a target="_blank" href="{{ route('encode-url', [$entry->slug, 'linkedin', 'linkedin-cam-aug-20', base64_encode(URL::current())]) }}" class='btn btn-info'>Linkedin</a>
</div>