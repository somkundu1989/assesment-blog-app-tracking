<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;
use App\Models\Shorturl;
use Hash;

class Common
{
	private function encrypt(String $string) {
	  $key = env('APP_KEY');  // "MAL_979805"; //key to encrypt and decrypts.
	  $result = '';
	  $test = "";
	   for($i=0; $i<strlen($string); $i++) {
	     $char = substr($string, $i, 1);
	     $keychar = substr($key, ($i % strlen($key))-1, 1);
	     $char = chr(ord($char)+ord($keychar));

	     $test[$char]= ord($char)+ord($keychar);
	     $result.=$char;
	   }

	   return urlencode(base64_encode($result));
	}

	private function decrypt(String $string) {
	    $key = env('APP_KEY');  //key to encrypt and decrypts.
	    $result = '';
	    $string = base64_decode(urldecode($string));
	   for($i=0; $i<strlen($string); $i++) {
	     $char = substr($string, $i, 1);
	     $keychar = substr($key, ($i % strlen($key))-1, 1);
	     $char = chr(ord($char)-ord($keychar));
	     $result.=$char;
	   }
	   return $result;
	}

    public static function urlBuilder(string $slugOrId, String $medium, String $campaign,String $reference)
    {
    	$queryParam = '?utm_source='. url('/').'&utm_medium='.$medium.'&utm_campaign='.$campaign.'&utm_reference='.$reference;
    	$onlyUrl=route('frontend.detail', [$slugOrId]);
        $finalUrl= $onlyUrl.$queryParam;

        $url_coded = $this->encrypt($finalUrl, $key);	# $url_decoded = $this->decrypt($url_coded, $key);
        
        $now = date("Y-m-d H:i:s");

        Shorturl::create(['url'=>$finalUrl,
			        	'hash'=>Hash::make($now.$slugOrId.$medium.$campaign.$reference.rand(1,10000000)),
			       		'created_at'=>$now
			       	]);




    }
}