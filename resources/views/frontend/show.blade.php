@extends('layouts.app')

@section('content')
<div class="container-fluid">
    @include('frontend.show_fields')

  
    <div class="clearfix">
    	<a href="{{ route('frontend.index') }}" class="btn btn-info">Back</a>
    </div>

    <div class="clearfix"><br /></div>
    
    <div class="clearfix">
        

            @include('frontend.action')
           
    </div> 
</div>
@endsection
