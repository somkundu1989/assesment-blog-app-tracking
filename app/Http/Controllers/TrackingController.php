<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTrackingRequest;
use App\Http\Requests\UpdateTrackingRequest;
use App\Models\Shorturl as Tracking;
use Illuminate\Http\Request;
use Flash;
use Response;

class TrackingController extends Controller
{
    /**
     * Display a listing of the Tracking.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Tracking $trackings */
        $trackings = Tracking::orderBy('created_at','desc')->paginate(env('PAGINATION_PERPAGE'));

        return view('trackings.index')
            ->with('trackings', $trackings);
    }

    /**
     * Display the specified Tracking.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($hashcode)
    {
        /** @var Tracking $tracking */
        $tracking = Tracking::where('hashcode',$hashcode)->first();

        if (empty($tracking)) {
            Flash::error('Tracking not found');

            return redirect(route('trackings.index'));
        }

        return view('trackings.show')->with('tracking', $tracking);
    }

}
