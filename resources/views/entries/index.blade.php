@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="float-left">Dairy Entries</h5>
                    <div class="float-right"> 
                        <div class="form-group">
                            <a href="?view=tableview" class='btn btn-primary'>Table</a>
                            <a href="?view=gridview" class='btn btn-primary'>Grid</a>
                            <a href="?view=timeline" class='btn btn-primary'>Timeline</a>
                        </div>

                    </div>
                </div>

                <div class="card-body">

                    <section class="content-header">
                        <h1 class="pull-right">
                           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{ route('entries.create') }}">Add New</a>
                        </h1>
                    </section>
                    <div class="content">
                        <div class="clearfix"></div>

                        @include('flash::message')

                        <div class="clearfix"></div>
                        <div class="box box-primary">
                            <div class="box-body">
                                @if(session('view') && session('view')=='tableview')
                                    @include('entries.views.table')
                                @endif

                                @if(session('view') && session('view')=='gridview')
                                    @include('entries.views.grid')
                                @endif
                                    
                                @if(session('view') && session('view')=='timeline')
                                    @include('entries.views.timeline')
                                @endif
                                @if(!session('view'))
                                    @include('entries.views.grid')
                                @endif
                            </div>
                        </div>
                        <div class="text-center">
                        
                        {{ $entries->links() }}

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

